
package edu.ifpb.pp.aulajsf;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author Ricardo Job
 */
@Named(value = "gerenciadorPessoa")
@SessionScoped
public class GerenciadorPessoa implements Serializable {
    
    private Pessoa pessoa;
    
    public GerenciadorPessoa() {
        
        pessoa = new Pessoa();
        
    }

    public Pessoa getPessoa() {
        return pessoa;
    }
    
    
    
}
